﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carps.DBLogging
{
    public class DiLogging : IDbLogging
    {
        public void UpdateLog(SqlCommand comm, Log l)
        {
            if (l.Application == null)
            {
                l.Application = "";
            }

            if (l.Version == null)
            {
                l.Version = "";
            }

            if (l.Module == null)
            {
                l.Module = "";
            }

            if (l.SourceFunction == null)
            {
                l.SourceFunction = "";
            }

            if (l.UserProfile == null)
            {
                l.UserProfile = "";
            }

            if (l.ErrDescription == null)
            {
                l.ErrDescription = "";
            }

            if (l.SqlText == null)
            {
                l.SqlText = "";
            }

            if (l.Details == null)
            {
                l.Details = "";
            }

            comm.CommandText =
                "INSERT INTO CarpsLog (TimeStamp, Application, Version, Module, SourceFunction, UserProfile, ErrDescription, SQLtext, Details)" +
                "VALUES (GetDate(), @Application, @Version, @Module, @SourceFunction, @UserProfile, @ErrDescription, @SQLtext, @Details)";

            comm.Parameters.Add("@Application", l.Application);
            comm.Parameters.Add("@Version", l.Version);
            comm.Parameters.Add("@Module", l.Module);
            comm.Parameters.Add("@SourceFunction", l.SourceFunction);
            comm.Parameters.Add("@UserProfile", l.UserProfile);
            comm.Parameters.Add("@ErrDescription", l.ErrDescription);
            comm.Parameters.Add("@SQLtext", l.SqlText);
            comm.Parameters.Add("@Details", l.Details);

            comm.ExecuteNonQuery();

            comm.Parameters.Clear();
        }
    }
}
