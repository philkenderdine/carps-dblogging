﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carps.DBLogging
{
    public interface IDbLogging
    {
        void UpdateLog(SqlCommand comm, Log l);
    }
}
