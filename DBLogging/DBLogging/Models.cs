﻿namespace Carps.DBLogging
{
    public class Log
    {
        /// <summary>
        /// The application that the log was generated on
        /// </summary>
        public string Application { get; set; }
        /// <summary>
        /// The version of the application
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// The class/module that was executing when the log was generated
        /// </summary>
        public string Module { get; set; }
        /// <summary>
        /// The function that was executing when the log was generated
        /// </summary>
        public string SourceFunction { get; set; }
        /// <summary>
        /// The SQL text that was executing or previously executed when the log was generated
        /// </summary>
        public string SqlText { get; set; }
        /// <summary>
        /// The user group ID that was active when the log was generated
        /// </summary>
        public string UserProfile { get; set; }
        /// <summary>
        /// Human readable string error description
        /// </summary>
        public string ErrDescription { get; set; }
        /// <summary>
        /// Further error details (exception text, integration message, etc)
        /// </summary>
        public string Details { get; set; }
    }
}
